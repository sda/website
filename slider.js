$(document).ready(function(){
    $('.slider').slick({
        infinite: true,
        slidesToShow: 5,
        autoplay: true,
        autoplaySpeed: 2000,
    });
});